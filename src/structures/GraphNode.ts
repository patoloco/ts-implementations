export class GraphNode {
  private data: any;
  private from: GraphNode[];
  private to: GraphNode[];

  constructor(data?: any) {
    this.data = data;
    this.from = [];
    this.to = [];
  }

  public connectToOneWay(node: GraphNode): boolean {
    let connected = false;
    if (!this.to.includes(node)) {
      this.to.push(node);
      connected = true;
    }

    if (!node.from.includes(this)) {
      node.from.push(this);
      connected = true;
    }

    return connected;
  }

  public connectFromOneWay(node: GraphNode): boolean {
    let connected = false;

    if (!node.to.includes(this)) {
      node.to.push(this);
      connected = true;
    }

    if (!this.from.includes(node)) {
      this.from.push(node);
      connected = true;
    }

    return connected;
  }

  public connectTwoWays(node: GraphNode): boolean {
    let connected = false;

    if (this.connectToOneWay(node)) {
      connected = true;
    }

    if (this.connectFromOneWay(node)) {
      connected = true;
    }

    return connected;
  }

  public removeConnectionTo(node: GraphNode): boolean {
    let removed = false;
    const n = this.to.indexOf(node);
    if (n !== -1) {
      // connection to exists
      this.to.splice(n, 1);
      removed = true;
    }

    const m = node.from.indexOf(this);
    if (m !== -1) {
      // connection from exists
      node.from.splice(m, 1);
      removed = true;
    }

    return removed;
  }

  public removeConnectionFrom(node: GraphNode): boolean {
    let removed = false;
    const n = this.from.indexOf(node);
    if (n !== -1) {
      // connection to exists
      this.from.splice(n, 1);
      removed = true;
    }

    const m = node.to.indexOf(this);
    if (m !== -1) {
      // connection from exists
      node.to.splice(m, 1);
      removed = true;
    }

    return removed;
  }

  public removeConnections(node: GraphNode): boolean {
    let removed = false;

    if (this.removeConnectionTo(node)) {
      removed = true;
    }
    if (this.removeConnectionFrom(node)) {
      removed = true;
    }

    return removed;
  }

  public getData(): any {
    return this.data;
  }

  public getToNodes(): GraphNode[] {
    return this.to;
  }

  public getFromNodes(): GraphNode[] {
    return this.from;
  }

  public isConnectedTo(node: GraphNode): boolean {
    return this.to.includes(node);
  }

  public isConnectedFrom(node: GraphNode): boolean {
    return this.from.includes(node);
  }

  public setData(data: any) {
    this.data = data;
  }
}
