import { ListNode } from "./ListNode";
import { ListSearchResponse, SinglyLinkedList } from "./SinglyLinkedList";

export class CircularLinkedList<T> extends SinglyLinkedList<T> {

  constructor() {
    super();
  }

  public pushEnd(data: T) {
    if (this.isEmpty()) {
      const tmp = new ListNode<T>(data);
      this.head = this.tail = tmp;
      this.getHead().setNext(this.head);
    } else {
      const tmp = new ListNode<T>(data);
      this.tail.setNext(tmp);
      this.tail = tmp;
      this.tail.setNext(this.head);
    }
    this.length++;
  }

  public pushFront(data: T) {
    const tmp = new ListNode<T>(data);
    if (this.isEmpty()) {
      tmp.setNext(tmp);
      this.head = this.tail = tmp;
      this.length++;
    } else {
      tmp.setNext(this.head);
      this.tail.setNext(tmp);
      this.head = tmp;
      this.length++;
    }
  }

  public insertAfterPrimitive(data: any, prim: any): boolean {
    const search = this.search(prim);
    if (!search.node || !search.found) {
      return false;
    }

    const newNode = new ListNode<T>(data);
    const currentNode = search.node;
    const nextNode = currentNode.getNext();

    // single item list
    if (this.length === 1) {
      newNode.setNext(this.head);
      this.tail = newNode;
      this.head.setNext(newNode);
      // multi item list
    } else if (currentNode !== this.tail) {
      currentNode.setNext(newNode);
      newNode.setNext(nextNode);
    } else {
      // is tail
      newNode.setNext(nextNode);
      currentNode.setNext(newNode);
      this.tail = newNode;
    }

    this.length++;
    return true;
  }

  public insertBeforePrimitive(data: T, prim: any): boolean {
    const search = this.search(prim);
    if (!search.node || !search.prev || !search.found) {
      return false;
    }

    const newNode = new ListNode<T>(data);
    // single item list
    if (this.length === 1) {
      newNode.setNext(this.head);
      this.head.setNext(newNode);
      this.tail = this.head;
      this.head = newNode;
      // multi item list
    } else if (search.node !== this.getHead()) {
      const currentNode = search.node;
      const prevNode = search.prev;
      prevNode.setNext(newNode);
      newNode.setNext(currentNode);
      // is head
    } else {
      newNode.setNext(this.head);
      this.tail.setNext(newNode);
      this.head = newNode;
    }

    this.length++;
    return true;
  }

  public getList(): T[] {
    if (this.length === 0) {
      return [];
    }

    const data: any[] = [];
    let tmp: any;
    tmp = this.head;
    data.push(tmp.getData());

    for (let i = 1; i < this.length; i++) {
      tmp = tmp.getNext();
      data.push(tmp.getData());
    }

    return data;
  }

  public search(val: any, key?: string): ListSearchResponse<T> {
    let node: any = this.head;
    const headData = node.getData();
    let depth = 0;

    if ((headData && key && headData[key] && headData[key] === val) || (!key && headData === val)) {
      return { depth, node, prev: this.tail, found: true } as ListSearchResponse<T>;
    }

    let prev: any;
    for (let i = 1; i < this.length; i++) {
      prev = node;
      node = node.getNext();
      depth++;
      const nodeData = node.getData();
      if ((nodeData && key && nodeData[key] && nodeData[key] === val) || nodeData === val) {
        return { depth, node, prev, found: true } as ListSearchResponse<T>;
      }
    }

    return { found: false } as ListSearchResponse<T>;
  }

  public searchAndDelete(val: any, key?: string): any {
    const item = this.search(val, key);
    if ((typeof(item.found) === "boolean" && item.found === false) || !item.node || !item.prev) {
      return false;
    }

    const current = item.node;
    const prev = item.prev;
    const next = current.getNext();

    // only item in list
    if (this.length === 1) {
      const tmp = new ListNode<T>(null);
      this.head = this.tail = tmp;
      this.head.setNext(this.head);
    } else {
      prev.setNext(next);
      current.setNext(null);
      if (this.head === current && next) {
        this.head = next;
      } else if (this.tail === current && prev) {
        this.tail = prev;
      }
    }

    this.length--;
    return true;
  }

  public deleteFirst(): void {
    if (this.getLength() === 0) {
      return;
    } else if (this.getLength() === 1) {
      const tmp = new ListNode<T>(null);
      this.head = this.tail = tmp;
      this.length--;
      return;
    }

    const next = this.head.getNext();
    this.head.setNext(null);
    this.tail.setNext(next);
    if (next) {
      this.head = next;
    }
    this.length--;
  }

}
