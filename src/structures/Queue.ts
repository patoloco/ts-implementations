export class Queue {
  protected q: any[];

  constructor() {
    this.q = [];
  }

  public cutLine(...items: any[]): void {
    if (items && items.length > 0) {
      this.q = items.concat(this.q);
    }
  }

  public isEmpty(): boolean {
    return this.q.length === 0;
  }

  public length(): number {
    return this.q.length;
  }

  public load(...items: any[]): void {
    if (items && items.length > 0) {
      for (let i = 0, len = items.length; i < len; i++) {
        this.q.push(items[i]);
      }
    }
  }

  public next(): any {
    if (!this.isEmpty()) {
      return this.q.shift();
    }
  }

  public reset(): void {
    this.q = [];
  }

  public unloadAll(): any[] {
    const tmp = this.q.slice();
    this.q = [];
    return tmp;
  }

  public view(index?: number) {
    if (!index && index !== 0) {
      return this.q.slice();
    }
    if (this.q[index]) {
      return this.q[index];
    }
    return null;
  }

}
